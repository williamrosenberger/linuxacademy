AWSTemplateFormatVersion: '2010-09-09'
Description: >
  Networking definitions for within the VPC.

Parameters:
  EnvironmentName:
    Description: An environment name that is prefixed to resource names
    Type: String

  VpcCIDR:
    Description: IP range (CIDR notation) for this VPC
    Type: String
    Default: 10.192.0.0/16
  
  PublicSubnetCIDR:
    Description: IP range (CIDR notation) within the VPC for routable instances
    Type: String
    Default: 10.192.10.0/24
  
  PrivateSubnetCIDR:
    Description: IP range (CIDR notation) for the private subnet
    Type: String
    Default: 10.192.20.0/24
  
  SSHLocation:
    Description: The IP address range that can be used to SSH to the VPC
    Type: String
    MinLength: '9'
    MaxLength: '18'
    Default: 0.0.0.0/0
    AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
    ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.

Resources:
  VPC:
    # Virtual personal cloud in which all compute instances will be defined.
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  InternetGateway:
    # The internet gateway provides access to the wider internet. Without
    # an IGW, traffic cannot be routed to the internet. By adding an IGW,
    # we can allow certain nodes (specifically, nodes in a public subnet) to
    # reference IP addresses in networks other than this VPC.
    # In order to use an IGW, nodes must have a public IP. In order to give
    # private nodes internet access, we'll need to use a `NatGateway`.
    # See [here](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html)
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  InternetGatewayAttachment:
    # Relate the IGW to our VPC.
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC
  
  PublicRouteTable:
    # Add a route table to our VPC so that
    # we can specify where traffic should be routed.
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  DefaultPublicRoute:
    # Declare the outgoing routing rule. All traffic that hits
    # the IGW should be routed to the internet.
    Type: AWS::EC2::Route
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway

  ExternalSSHAccess:
    # This security group allows IP addresses from the specified external network
    # (`!Ref SSHLocation`) to SSH into the instances that are labeled with this
    # security group (assuming those nodes have a public IP address, e.g. the
    # bastion node).
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable SSH access via port 22 from external network
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !Ref SSHLocation
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  InternalSSHAccess:
    # This security group allows IP addresses from within the VPC
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Enable SSH access via port 22 from internal VPC network
      VpcId: !Ref VPC
      SecurityGroupIngress:
        # From bastion nodes
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !Ref PublicSubnetCIDR
        # From internal nodes.
        - IpProtocol: tcp
          FromPort: '22'
          ToPort: '22'
          CidrIp: !Ref PrivateSubnetCIDR
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName
  
  PublicSubnet:
    # This subnet defines the IP range where publicly accessible nodes should live.
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !Ref PublicSubnetCIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName
  
  PublicSubnetRouteTableAssociation:
    # Define which route table the public subnet should use.
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet
  
  PrivateSubnet:
    # Subnet that should be used for private, non-routable IP addresses.
    # In order for instance within this subnet to have internet access,
    # that access must be NATted (using, e.g., a NatGateway).
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref PrivateSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName
  
  NatGatewayEIP:
    # Define the publically accessible IP address that should be used by
    # private nodes. That is, this is the IP adress that is used for NATting
    # private instances.
    Type: AWS::EC2::EIP
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  NatGateway:
    # Define network address translation so that private nodes can
    # access the internet.
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGatewayEIP.AllocationId
      SubnetId: !Ref PublicSubnet
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  PrivateRouteTable:
    # Add a route table to the VPC that will be used to record how to
    # route traffic within the VPC.
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Ref EnvironmentName

  DefaultPrivateRoute:
    # Declare that all traffic coming from a private instance and
    # destined for the internet should be routed through the nat gateway.
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway

  PrivateSubnetRouteTableAssociation:
    # Define which route table the private subnet should use.
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet

Outputs:
  VPC:
    Description: The created VPC
    Value: !Ref VPC

  PrivateSubnet:
    Description: Private VPC subnet
    Value: !Ref PrivateSubnet
  
  PublicSubnet:
    Description: Private VPC subnet
    Value: !Ref PublicSubnet

  SgExternalSSHAccess:
    Description: >
      Security group for SSH access from external sources.
    Value: !Ref ExternalSSHAccess

  SgInternalSSHAccess:
    Description: >
      Security group for SSH access from internal sources.
    Value: !Ref InternalSSHAccess
