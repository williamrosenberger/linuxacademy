
# Cloud Formation intro

- Infrastructure as code
- Supports JSON and YAML
    - Console will let you convert between them.
- "Cloud formation template" describes everything that will be created in the environment
    - Stored in S3 bucket.
- Templates generates stacks.
    - Templates are classes
    - Stacks are instances (objects)
- Stacks create resources that can be seen in the other interfaces. Using those interfaces to edit the resources is not a good practice.
    - "Stack drift"
- Permission for templates are managed with IAM roles

---

# IAM

- IAM policies are associated with users who can then use CF templates that use those policies.
- Always use principle of least privilege 
- Policies are defined with JSON in the Policies Console
- Policies can be attached to a group of users.
- The "Resource" key in the policiy defin allows you to select the specific things that the policy applies to. So, if you want to make sure devs can't upate prod, specify prod's ID in Resource for a deny policy.

---

- CloudFormation _may_ be uploaded to s3 or used directly from the command line.
    - Do not _need_ to upload to S3.
    - I guess this means you can git clone and then use the template?
- Service role
    - A service account.
    - Lets CF make calls for you.
        - E.g., use Lambda to get an AMI ID.
    - Defined in IAM Roles

---

# Resource types

- CF does not support every AWS service.
- List: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html
- Might be able to use CF Custom Resources
- Could write a Lambda function to do my work for me.
    - Oh, he just mentioned Lambda in this section. Maybe this is the approach?

---

# Templates in depth

- This URL provides docs for all supported resources:
    - https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html
- Only the "resources" section is required.
- `Parameters`
    - This section provides configuration/details for the template.
    - AWS builds its UI based on these parameters.
- `Mappings`
    - Let you select specific values based on the value of another variable.
- `Conditions`
    - Create different resources in different contexts.
    - Dev vs. prod.
- `Resources::Metadata`
    - Allows you to install software and do configuration on resources.
- `Transform`
    - Condense templates
    - Can be a Lambda function that does... whatever.
    - For example, find an AMI ID.
    - Also, `include`s. Can include other templates.
- `Output`
    - Results of the template.
    - E.g., URL to the website that was built

---

# Intrinsic functions

- Built-in funcitons for defining dynamic values.
- `Fn::`
    - `yaml` has a short form: The `!` syntax
- `Fn::Base64`
    - base 64 represention of input string
    - `!Base64 CloudFormation`
- `FindInMap`
    - Key->value lookup for something in the `mappings` section.
- `Cidr`
    - Returns an array of CIDR address blocks
    - `!Cidr [ipblock, count, cidrbits]`
    - Splits an existing CIDR range.
- `GetAtt`
    - Get a value from a resource
    - `!GetAtt resourceName.AttributeName`
    - `!GetAtt WebServerInstance.PublicDnsName`
- `GetAZs`
    - Get the AZs in the user's region.
    - E.g., `!GetAZs us-east-1`
    - Use the current region: `!GetAZs ""`
- `ImportValue`
    - Use value of another template.
- `Join`
    - Append values.
    - `!Join [ delimiter, [value1, value2]]`
- `Select`
    - Get a value from a list (by index)
    - Indexing starts at 0
- `Split`
    - Split a string based on a delimiter
- `Sub`
    - Substitue values into a string
    - !Sub "a_string${sub}"
- `Transform`
    - Specifices a macro to perform custom processing.
- `Ref`
    - values of speicifed value or resource.

# Pseudo parameters

- Global variables
- Access with `!Ref`
- `AWS:Region`
- `AWS:AccountId`
- `AWS:NotificationARNs`
- `AWS:NoValue`
    - Can use this to remove values (e.g., using `!if`)
    - Is not `null`. It means "Drop the key/value entirely".
- `AWS:Partition`
    - Are you in regular AWS? China AWS? GovCloud AWS?
- `AWS:StackId`
- `AWS:StackName`
- `AWS:URLSuffix`

# Condition functions

- Boolean operators
- `!If`
- Remember: `!`s are YAML short form. They don't mean "Not" like in many languages.

---

# Template best practices

- Reuse templates with imports
- Do not add credentials to templates. Use params.
    - Can use `NoEcho` to obfuscate private parameters.
- Use parameter constraints.
- Templates are _code_. Check it in to Git. Use code review. Comment. Document.

---

# Stacks

- If a delete fails, there are probably still resources sitting around, spending $.
- Deleted stacks are recorded for 90 days.

Protecting stacks
- Termination protection
    - Prevents stack deletion until the setting is disabled.
    - Termination protection cascades down to nested stacks
    - Disabled by default
    - Doesn't keep people going over to, for example, EC2 and manually deleting resources.
- Stack-level policies
    - JSON docs that define the update polciies that can be performed on specific resources in the stack.
    - Can only define one stack policy per stack.
- Resource-level policies
    - Deletion policy
        - Can be specified on each resource in a stack. For example, don't delete the contents of an S3 bucket.
        - Delete (default)
            - Dependencies are respected. If you "Retain" a child of something you're "Delete"ing (e.g., delete a security group of a EC2 instance that you're retaining), the delete will fail.
        - Retain
        - Snapshot: Some resources (e.g., RDS) can take snapshots.

# Rollback triggers

- Set cloudwatch alarms that, if triggered, will cause the stack to be rolled back.
- Rollback period can be anywhere from 0 to 180 minutes.
    - 0 minutes means "only monitor while creating new stack"
- Max of 5 rollback triggers
- Missing CW triggers cause CF deploy to fail.

# AWS Config

- Track historical config of stacks
- Similar to Drift Detection.
    - They can work together.
- Can be configured to SNS message when something changes.
- In AWS Config console
    - settings -> recording -> turn on

# Drift detection

- Drift occurs when the config of the running stack is no longer what's in the template.
    - E.g., when you manually edit resources without updating the stack.
- Drift detectoin can be done on an entire stack and on a specific resource.
    - E.g., check stack for drift, then check the drifted resources for what specifically changed.
- Vs. AWS Config?
    - Can set up drift detection
        using AWS config.
    - AWS config: use managed rule "CF Stack drift detection check"
        - You'll need to set up an AWS role to allow AWS COnfig to run configs.

# Drift remediation

- Can just manually remove changes to resources
- Or, can update the template and run a stack update if you want to keep the changes.

# VPC Endpoints

- What are they? Privately connect VPC to services powered by privatelink w/out requiring an internet gateway
    - e.g., connect to S3
    - Instances don't need a public IP to be able to communicate with these services if you're using a VPC endpoint.

# Updating stacks

- Change sets
    - Examine changes before running an update.
- Should do drift detection before doing updates
    - Updates will _work_ with drift, but it's always good to minimize the number of unknowns.
- Update behaviors  
    1. Update w/ no interruption
    1. Update w/ some interupption
        - Things are interrupted, but the ID is kept.
    1. Replacement
        - Resource is recreated with a new ID.
    - Resources decide what their update behavior is. See documentation.
- Update rollback
    - If a portion of an update fails, all changes roll back.
- Stack policies
    - Can be used to protect resources from unintentioanl update.
    - If a policy is activated, all resources are protected unless you explicitely say a resource can be updated.
    - What if you've thought things through and you _actally_ want to make the change?
        - Policy can be overridden during update. When you submit your template to create a stack, just edit the policy.
- Change sets
    - List of changes that will occur
    - Do not indicate that the changes will succeed or not. Only indicate what will be attempted.
    - Change sets are relative to the current state. So, if you create 2 change sets against the same stack, then run the first change set, the second one will be removed (because the stack it was defined relative to no longer exists).
- Rolling update
    - Can, for example, guarantee that at least one instance of a resource will always be up during the update.

# Cross-stack references

- Let you create resources in separate stacks, export properties, then use those properties in other stacks.
- Let's you build service-oriented stacks.
- Use the `Outputs` section to create an output of your template. To export this output, use `Export`.
- Use `!ImportValue <value name>` to import somehting someone else has exported.
- Order of stack creation matters. Exporters must be run first.
- Can't delete a stack if another stack references its outputs. Need to delete the importer first.

# Nested stacks

- Stacks created to be used as part of other stacks
- "Child" and "parent" stacks. Can have multiple layers.
- Use `AWS::CloudFormation::Stack` within the parent stack to create a child stack.
    - Has a `TemplateURL` property that references the S3 location of the child stack.
- Can delete the parent stack w/out first deleting that child (unlike cross-stack refs)
    - this isn't recommended

# AWS CLI

- Primary distribution is pip 
    - `pip install awscli`

- Configuring
    - `aws configure`
    - Access keys are in IAM
        - Best practice is to _delete_ your access keys!
        - Create a new one when you need it.

- `aws cloudformation list-stacks`
- `aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE`
- `aws cloudformation create-stack --stack-name <name> --template-body <file> --parameters ParameterKey=<param_name>,ParameterValue=<value> ParameterKey=<param_name2>,ParameterValue=<value2>`

# Bootstrapping

- Bootstrapping lets you load instances with software, start services, etc.
- A repeatable process for running something on AWS.
- It's not always practical to pre-bake everything into an AMI
- Typical operations
    - Mount drives, start services, update files, get latest version of software, register w/ ELB
- When manually creating an EC2 instance, you can specify `User Data`. This is a bootstrapping script.
    - These can be specified in CF templates.
- CloudFormation can define the following:
    - `cfn-init`
        - Interprets resource metadata to do initial setup (package install, file create, service starts, ...)
    - `cfn-signal`
        - Signals CloudFormation's `WaitCondition` for synchronizing with other resources that are being created.
    - `cfn-get-metadata`
        - Retrieves metadata about the resource
    - `cfn-hup`
        - Daemon that checks for metadata and executes hooks when something changes.

# Wait conditions

- Frequently used in with hybrid environments when you need to wait for local resources to spin up.
- When working with EC2, the recommendation is to use creation policies instead
- What are they? They pause execution and wait for signals before continuing
- `AWS::CloudFormation::WaitCondition`
    - List for `WaitConditionHandle` events to be received. When the specified number of these
        are received, things continue
- `AWS::CloudFormation::WaitConditionHandle`
    - This is the triggering mechanism that lets you signal the condition
    - Define them as a separate resource
    - Then reference them in, for example, `UserData` of an EC2 instance.
- In the background, these elements are setting up a URL endpoint that's hit by the handler to signal the condition.

# Creation Policies

- Pause the creation of a resource until a specified number of signals are received.
- Allows you to indicate when, for example, an app is fully installed on an EC2 instance.
- UNlike wait conditions, creation policies are an attribute of the resorce itself
- Can also be used with autoscaling groups to wait for X number of EC2 instances to come up.

# Update policy

- Let's say you need to update an autoscaling group and you need to update the launchconfig. How do you do this without down time?
- Update Policies are an attribute of an autosacling group and are resposnible for managing updates to that autoscaling group.
- Three types:
    - `AutoScalingReplacingUpdate`
        - Specifies to replace an instance in an autoscaling group
        - If `WillReplace` is true, it will replace the whole autoscaling group and instances.
        - For the sake of rollbacks, the old autoscaling groups will be kept around for ashort time.
    - `AutoScalingRollingUpdate`
        - Updating some of the instances, but doesn't stomp on everything all at once.
    - `AutoScalingScheduledAction`
        - Used when you have predicatble load patterns
        - Allows you to schedule increases/decreases in group size.

# Helper scripts

- Python scripts to help setup EC2 instances (e.g., install software, start services)
- Work based on metadata defined on your template.
- They're pre-installed on AWS Linux AMIs.
    - They cmoe from the package `aws-cfn-bootstrap`.
    - Recommendation is to update them in your `userdata` before using them.
- Example: `/usr/aws/bin/cfn-init`
- You need to include calls to execute them in your `userdata`
- `cfn-init`
    - Interprets metadata to install packages, create files, start services.
- `cfn-signal`
    - Signals CF `WaitCondition`
- `cfn-get-metadata`
    - Gets metadata that's defined for a given resource. Prints it to `stdout`.
- `cfn-hup`
    - Daemon that listens for metadata update and fire hooks when changes are detected.

# Systems manager parameter store

- Allow custimization of templates
- Make your template dynamically configurable
- Centralized place to update parameters.
- Param store lets you abstract values out of your templates, allwoing for better security and reusability.

Use:
- In your template's `Parameters` section, you can specify like this:

``` yaml
Parameters:
    ParamName:
        Type: 'AWS::SSM::Parameter::Value<String>'
        Default: devInstance
```

# Dynamic references

- You can use parameter sotre references in more pl;aces than just the `Parameters` section of your template
- Dynamic references can be used with the following reference patterns:
    - `ssm` for plaintext values in the Systems Manager Parameter Store
    - `ssm-secure` for secure strings in the Systems Manager Parameter Store
    - `secretsmanager` for secrets stored in AWS Secrets Manager
- Can use up to 60 dynamic refernces
- Dynamic parameter syntax is of the form `{{ resolve:ssm:<parameter-name>:<version> }}`

# Secrets Manager

- Similar to Parameter Store
- Parameter store is free. Secrets manager costs.
- Has the ability to automatically rotate secrets
- Secrets can be shared across accounts
- Reference with: `{{ resolve:secretsmanager:MyRDSSecret:SecretString:password }}`
- Create with:
    ``` yaml
    Resources:
        MyRDSInstanceRotationSecret:
            Type: AWS::SecretsManager::Secret
            Properties:
                Description: "Secret for RDS"
                GenerateSecretString:
                    SecretStringTemplate: '{"username": "admin"}'
                    GenerateStringKey: 'password'  # Automatically generated password
                    PasswordLength: 16
                    ExcludeCharacters: '"@/\'
    ```

# Macros

- Preprocess the cloudformation template
- This happens before CF starts creating resources
- To create:
    ``` yaml
    # Create a Lambda function
    Resources:
        LambdaMacro:
            Type: AWS::Lambda:Function
    
    # Register the labda function as a macro
    NewMacro:
        Type: AWS::CloudFormation::Macro
        Properties:
            Name: "Description"
            FunctionName:
                Fn::GetAtt:
                - LambdaMacro
                - ARN
    
    # Use the macro
    Transform:
    - NewMacro
    ```

- Can also use a transform at the "snippet" level
    ``` yaml
    Resources:
        MyTable:
            Type: AWS::DynamoDB::Table
            Properties:
                TableName: CFDeepDive
                KeySchema: ...
                Fn::Transform:
                - Name: DynamoDBDefaults
    ```

# Custom resources

- CF does not support every resource in AWS. How do we handle cases where things fall out of what's supported?
- Custom resoruces let us define our own deployment logic.
- Implemented using labmda functions
    - Packaged in S3 buckets or inline.
- CF will pause execution while the Labmda function is running.
- CF templates will have the following elemnts to utilize Labda
    - Custom resrouces have one property:
        - Servvice token
    - Labmda execution role
    - Labmda function
        - Handler
        - Runtime
        - Role
        - Timeout

# Serverless application model

- Open-source framework for building serverless applications
- Uses CF syntax
- Uses a specific Transform function in the CF tempalte to apply the SAM stuff.

# Stack sets

- How do you deploy across regions and accounts? "Provisioning at scale"
- What problems do multi-regions and multiaccount deploments have?
    - Manual operations introdue inconsistencies
    - Increased maintenance workload
    - Third-party tools introduce more complexity and cost
    - Example of complexity: Cross-region means you need to deal with time zones
- Solution: Stack sets
- Standardize deployments across regiobns and accounts
- Two types of accounts:
    - Admin account
        - Create the stack set to deploy to target accounts
    - Target accounts
- Can create a max of 20 stack sets, and max of 500 stack instances per stack set
    - So, a max of 1500 stack instances running in a given region for each admin account.

---

# Summary and best practices

- Organize stacks by lifecycle and ownership
    - E.g., DB team, Fronteend team, securioty team
    - Use cross-stack refernces to coordinate between the stacks
    - Common frameworks:
        - Multlayered: Upper layers depend on products of lower layers
        - Service-oriented: Interdependent services communicating between eachother.
- Use IAM to control access to IAM!
- Reuse templates to replicate infrastructure
    - Craete environments for dev, test, ...
    - Use nested stacks to reuse common patterns
- Do not hard-code credentials
- Use parameter constraints
- Keep helper scripts up to date (`yum install -y aws-cfn-boostrap`)
- Validate your templates before running them.
- Don't drift your stack!
- Use change sets when updating stacks.
- Use stack policiies to protect critical resources
- Treat your templates as software! Use version control, code review, ...
- Update your EC2 instances regularly (`yum update`).