
# Elasticsearch Certification Preparation course

- Master-eligible nodes
    - Masters coorindate the cluster
    - Ensures the data is evenly distributed
    - Coordinates traffic
- Data nodes
    - Store data
    - Perform the requested operations

- Indexes
    - Split into shards
    - Primary shard
        - Can have multiple primary shards
    - Replica shard
        - Cannot exist on the same node as the primary.

- When all primary and replica shards are allocated, we're in "green state"
- When missing replica shards, we're in "yellow state"
- When missing primary shards, we're in "red state"

# Deploying Elasticsearch

On all nodes:
- Create elastic user
    - `sudo useradd elastic`
- Configure limits of the numb,er of open files
    - `sudo vim /etc/security/limits.conf`
    - Add `elastic - nofile 65536`
        - `-` means soft and hard limit
- `sudo vim /etc/sysctl.conf`
    - `vm.max_map_count = 262144`
    - `sudo sysctl -p`
- Download ES archive
    - `sudo su - elastic`
    - `curl -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.2.1-linux-x86_64.tar.gz`
    - `tar -xzvf elasticsearch-7.2.1-linux-x86_64.tar.gz`
    - `mv elasticsearch-7.2.1 elasticsearch`
    - `cd elasticsearch`

Configuring:

On master:
- `vim config/elasticsearch.yml`
    ``` yaml
    cluster.name: c1
    node.name: master-1
    node.attr.zone: 1
    network.host: [_local_, _site_]
    cluster.initial_master_nodes: ['master-1']

    # Node roles
    node.master: true
    node.data: false
    node.ingest: false
    ```
- `vim config/jvm.options`
    - Change
        ```
        -Xms1g
        -Xmx1g
        ```
    - to:
        ```
        -Xms768m
        -Xmx768m
        ```

On the others:
- `vim config/elasticsearch.yml`
    ``` yaml
    cluster.name: c1
    node.name: data-1
    node.attr.zone: 1
    node.attr.temp: hot  # Let's you put frequently-used indexes here. Can also be `warm`
    network.host: [_local_, _site_]
    discovery.seed_hosts: ["<private ip addr of master node>"]
    cluster.initial_master_nodes: ['master-1']

    # Node roles
    node.master: false
    node.data: true
    node.ingest: false
    ```

Start elasticsearch
- `./bin/elasticsearch -d -p pid` (Do this on all nodes).
- Logs are in `logs/<clustername>`. Check for when the node is up.
- View added nodes with `curl localhost:9200/_cat/nodes?v`

Install Kibana
- `curl -O https://artifacts.ealstic.co/downloads/kibana/kibana-7.2.1-linux-x86_64.tar.gz`
- `cd kibana`
- `vim config/kibana.yaml`
    ``` yaml
    server.port: 80
    server.host: "<private IP of Kibana node (master-1)>"
    ```
- `sudo /home/elastic/kibana/bin/kibana --allow-root`
    - need to start as root because we're using port 80.
    - This runs in the foreground.

# Security

- Generate a CA that will be used to sign node-specific certs
    ``` bash
    $ cd config
    $ mkdir certs
    $ cd certts
    # Generate CA
    # Note: Relative paths start at elasticsearch install dir
    $ /home/elastic/elasticsearch/bin/elasticsearch-certutil ca --out config/certs/ca --pass elastic_la
    # Generate certs for each node
    $ /home/elastic/elasticsearch/bin/elasticsearch-certutil cert --ca config/certs/ca --ca-pass elastic_la --name master-1 --out config/certs/master-1 --pass elastic_la
    $ /home/elastic/elasticsearch/bin/elasticsearch-certutil cert --ca config/certs/ca --ca-pass elastic_la --name data-1 --out config/certs/data-1 --pass elastic_la
    $ /home/elastic/elasticsearch/bin/elasticsearch-certutil cert --ca config/certs/ca --ca-pass elastic_la --name data-2 --out config/certs/data-2 --pass elastic_la
    $ /home/elastic/elasticsearch/bin/elasticsearch-certutil cert --ca config/certs/ca --ca-pass elastic_la --name node-1 --out config/certs/node-1 --pass elastic_la
    # Get the certs where they need to go (config/certs on all nodes)
    ```
    - For in-cluster comms, a self-signed cert is fine. For client interaction, you'll need a cert signed by someone real.
- Setting up the keystore with the passwords
    - Do this on all nodes.
    ``` bash
    $ ./bin/elasticsearch-keystore add xpack.security.transport.ssl.keystore.secure_password
    Password: elastic_la
    $ ./bin/elasticsearch-keystore add xpack.security.transport.ssl.truststore.secure_password
    Password: elastic_la
    $ ./bin/elasticsearch-keystore add xpack.security.http.ssl.keystore.secure_password
    Password: elastic_la
    $ ./bin/elasticsearch-keystore add xpack.security.http.ssl.truststore.secure_password
    Password: elastic_la
    $ ./bin/elasticsearch-keystore list
    ```

- In `config/ealsticsearch.yaml`
    ``` yaml
    xpack.security.enabled: true
    xpack.security.transport.ssl.enabled: true
    xpack.security.transport.ssl.verification_mode: certificate
    xpack.security.transport.ssl.keystore.path: certs/master-1  # User correct path here
    xpack.security.transport.ssl.truststore.path: certs/master-1  # User correct path here
    ```

- Restart Elasticsearch on all nodes (`pkill -F pid; ./bin/elasticsearch -d -p pid`)

- Now, you need to add passwords for users:
    ``` bash
    $ ./bin/elasticsearch-setup-passwords interactive
    ```
- And, configure kibana to use the password (`config/kibana.yaml`)
    ```
    elasticsearch.password: "" 
    ```
- And, curl with: `curl -u elastic localhost:9200`

- Configure HTTP encryption (elasticsearch.yaml) (do this on all nodes
    ``` yaml
    xpack.security.http.ssl.enabled: true
    xpack.security.http.ssl.keystore.path: certs/master-1
    xpack.security.http.ssl.truststore.paht: certs/master-1
    ```
    - Restart Elasticsearch
    - Update Kibana's `elasticsearch.hosts` to use https
    - Update Kibana's `elasticsearch.ssl.verificationMode: none`

# Define role-based acces control - Elasticsearch Security

- Note: the default user `elastic` is the cluster's super user
- In Kibana->Security->Roles, you can create roles
- "Cluster Privileges"
    - Defines what aspects of your cluster the user can change
- "Run as privileges"
    - Lets a user submit a request as someone else. 
- "Index privileges
    - What indices can you make/edit?
    - Can use wildcards - e.g., `sample-*`

Creating users:
- kibana->security->Users
- Apply your roles
    - In order to use Kibana, you need the `kibana_user` role

You can create roles with the console:

```
GET _security/role/sample
DELETE _security/role/sample
POST _security/role/sample
{
    "indicies": [
        {
            "names": ["sample-*"],
            "privileges": ["read", "write", "delete"]
        }
    ]
}

POST _security/user/username
{
    "roles": ["superuser"],
    "full_name": "First Last",
    "email": "me@place.com"
}
```

# Define an index

Three main components of an index:
1. Aliases
1. mappings
1. settings

Alieases will be covered later

- Get indices with: `GET _cat/indices?v`
- Create an index with `PUT sample-1`
- Delete: `DELETE sample-1`

```
# Create some indices
PUT logs
{
    "mappings": {
        "properties": {
            # This tells ES that `geo` shuold be a geographic point0
            "geo": {
                "properties": {
                    "coordinates": {
                        "type": "geo_point"
                    }
                }
            }
        }
    },
    "settings": {
        "number_of_shards": 1,
        "number_of_replicats": 1
    }
}

PUT shakespeare
{
    "mappings": {
        "properties": {
            "speaker": {
                "type": "keyword"
            },
            "play_name": {
                "type": "keyword"
            },
            "line_id": {
                "type": "integer"
            },
            "speech_number": {
                "type": "integer"
            }
        }
    },
    "settings": {
        number_of_shards: 1,
        number_of_replicas: 1
    }
}

PUT bank
{
    "settings": {
        number_of_shards: 1,
        number_of_replicas: 1
    }
}
```

# Perform index, create, read, update, delete on documents

```
# Get current indexes with 
GET _cat/indeces?v

# Create docs with:
# Leave out id to get a random UUID
# Because we didn't specify mappings, the default types will be used.
POST sample-index/_doc/<id>
{
    "firstname": "haha",
    "lastname": "blah"
}

# Get a doc
GET sample-index/_doc/<id>

# Document update
POST sample-index/_update/<id>
{
    "doc": {
        "lastname": "New"
        "newfield": "A"
    }
}

# Scripted update
POST sample-index/_update/<id>
{
    "script": {
        "lang": "painless",
        "source": "ctx._source.remove('middleinitial')"
    }
}

DELETE sample-index/_update/<id>
```

Bulk ingests:
- Ingest files come in pairs of lines:
    - first line: metadata for doc 1
    - second line: doc 1
    - third line: meta for doc 2
    - ...

``` bash
# To ingest accounts.json:
$ curl -u elastic -k -H 'Content-Type: application/x-ndjson' -X POST 'https://localhost:9200/bank/_bulk?pretty' --data-binary @accounts.json > accounts_bulk.json
```

# Index aliases

- Just another name for an index
- Filtereed alias: Alaises that point to a subset of the actual index

```
# Create an alias
POST _aliases {
    "actions": [
        {
            "add": {
                "index": "bank",
                "alias": "accounts"
            }
        }
    ]
}

# Remove an alias
POST _aliases {
    "actions": [
        {
            "remove": {
                "index": "bank",
                "alias": "accounts"
            }
        }
    ]
}

# Create a filtered alias
# Create an alias
POST _aliases {
    "actions": [
        {
            "add": {
                "index": "shakespear",
                "alias": "Henry_IV",
                filter: {
                    "term": {
                        "play_name": "Henry IV"
                    }
                }
            }
        }
    ]
}
```

# Index templates

- For things like streaming time series data, itmakes sense to have a set of indices. E.g.,
    an index for hot data that purges off to an index for warm data.
    - doing this manually is hard to maintain.
- Index templatsd make this easier.
- Let's say we want to create a series of indices like this:
    ```
    PUT logs-2020-01-01
    PUT logs-2020-01-02
    PUT logs-2020-01-03
    PUT logs-2020-01-04
    PUT logs-2020-01-05
    ```

- Create an index template with:
    ```
    PUT _template/logs 
    {
        aliases: {
            "logs": {}
        },
        "mappings": {
            "properties": {
                field1: {
                    "type": "keyword"
                }
            }
        },
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 1
        },

        # If and index's name matches one of these regexes, that index will be
        # created based on this template.
        "index_patters": [
            "logs-*
        ]
    }
    ```

- Can get with:
    ```
    GET logs_sample
    ```

# Dynamic mapping

- A mapping is basically the schema of the index.
- E.g., "this field is a keyword type"
- By default, there is always dynamic mapping. That is, elasticsearch will
    decide for you what the field type should be.
- Can use dynamic templates to override Elasticsearch's decision process.
- Create a dynamic template with:
    ```
    PUT sample-2
    {
        "mappings": {
            "dynamic_templates": [
                # Maps string types into the keyword type
                {
                    "strings_to_keywords": {
                        "match_mapping_type": "string",
                        # Don't match the fields that will be mapped by a template below
                        "unmatch": "*_text",
                        "mapping": {
                            type: "keyword"
                        }
                    }
                },
                # Maps long types to integers
                {
                    "longs_to_integers": {
                        "match_mapping_type": "long",
                        "mapping": {
                            "type": "integer"
                        }
                    }
                },

                {
                    "strings_to_text": {
                        "match_mapping_type": "string",
                        "mapping": {
                            "type": "text",
                            "match": "*_text",
                            "mapping": {
                                "type": "text"
                            }
                        }
                    }
                }
            ]
        }
    }
    ```

# Updating and reindexing documents

- Simple example: clone an index. Not useful
    ```
    POST _reindex
    {
        "source": {
            "index": "bank"
        },
        "destination": {
            "index": "bank_new"
        },
        "script": {
            # Add modification script here
        }
    }
    ```

- Remote reindexing
    - Copy an index from another cluser
    - Configure the other cluster for remote reindexing:
        - `config/elasticsearch.yaml`
            ``` yaml
            # Add all IPs in the remote cluster here. Or a CIDR range, I assume.
            reindex.remote.whitelist: "ip-of-cluster-that-will-clone"
            reindex.ssl.verification_mode: certificate
            reindex.ssl.truststore.type: PKCS12
            reindex.ssl.keystore.type:  PKCS12
            reindex.ssl.truststore.path: certs/node-1
            reindex.ssl.keystore.path: certs/node-1
            ```
    - Run the remote reindex with:
    ```
    POST _reindex
    {
        "source": {
            "remote": {
                "host": "<ip of other cluster>:9200",
                "username: "elastic",
                "password: "passwd"
            },
            "index": "bank",
            "query": {
                "term": {
                    "gender.keyword": {
                        "value": "M"
                    }
                }
            }
        },
        "destination": {
            "index": "bank_new"
        },
        "script": {
            "lang": "painless",
            # Drop a field
            "source": "ctx._source.remove('gender')"
        }
    }
    ```

# Update by query

- Increment the version of all documents
    ```
    POST bank/bank_update_by_query
    ```
- Make a change to a document
    ```
    POST bank/_update_by_query
    {
        "query": {
            "term": {
                "gender.keyword": "F"
            }
        }
        "script": {
            "lang": "painless",
            "source": """
                ctx._source.balance += ctx._source.balance * 0.03;

                if (ctx._source.transactions == null) {
                    ctx._source.transactions = 1;
                } else {
                    ctx._source.transactions++;
                }
            """
        }
    }
    ```

# Ingest pipelines

- Pipelines are saved and you can use them throughout indexes
- In order to use pipelines, you need to have at least one ingest node.

```
PUT _ingest/pipeline/test_pipeline
{
    "description": "very descript",
    # There ar a _lot_ of processors. See the official docs
    "processors": [
        {
            "remove": {
                "field": "account_number"
            }
        },
        {
            "set": {
                "field": "_source.fullname",
                # This processor supports mustache notation.
                "value": "{{ _source.firstname }} {{ _source.lastname }}"
            }
        },
        {
            "convert": {
                "field" : "age",
                "type": "string
            }
        },
        {
            "script": {
                "lang": "painless",
                "source": """
                    if(ctx.gender == "M") {
                        ctx.gender = "male"
                    } else {
                        ctx.gender = "female"
                    }
                """
            }
        }
    ]
}
```

# Defining mappings

```
PUT sample-1
{
    "aliases": {
        # meh
    },
    "settings": {
        # meh
    },
    "mappings": {
        "properties": {
            "field-name": {
                "type": "keyword"
            },
            "bio": {
                # Unlike keywords, text fields are analyzed (tokenized and searched)
                "type": "text"
            },
            "age": {
                "type": "short"
            },
            "interest_rate": {
                "type": "scaled_float",
                "scaling_factor": 10
            },
            "geoip": {
                "type": "geo_point"
            },
            "ip": {
                type": "ip"
            },
            "is_member": {
                "type": "boolean"
            },
            "last_modified": {
                "type": "date"
            }
        }
    }
}
```

# Custom analyzer

- Breaks apart text for later analysis
- Built-in analyzer:
    ```
    POST _analyze
    {
        "analyzer": "standard",
        "text": "Three quick brown foxes"
    }
    ```
- Language-specific analyzers are much better (e.g., stripping out stop words)
- English analyzer:
    ```
    POST _analyze
    {
        "analyzer": "english",
        "text": "Three quick brown foxes"
    }
    ```

- Anatomy of an analyzer
    - Has three components:
        1. Character filters
        1. Tokenizers
            - Recieves a stream of characters and breaks them up into characters.
        1. Token filters

- Creating a custom analyzer
    ```
    PUT analysis
    {
        "mappings": {
            "properties": {
                "text": {
                    "type": "text",
                    "analyzer": "whitespace_lowercase"
                }
            }
        }
        "settings": {
            "analysis-1": {
                "analyzer": {
                    "whitespace_lowercase": {
                        # `whitespace` is an existing analyzer. Here, we're re-using its tokenizer.
                        "tokenizer": "whitespace",
                        "filter": ["lowercase"],
                    }
                }
                
            }
        }
    }
    ```

    ```
    PUT analysis-2
    {
        "mappings": {
            "properties": {
                "text": {
                    "type": "text",
                    "analyzer": "standard_emoji "
                }
            }
        }
        "settings": {
            "analysis": {
                "analyzer": {
                    "standard_emoji": {
                        "type": "custom",
                        "tokenizer": "standard",
                        "filter": ["lowercase", "english_stop"],
                        "character_filter": ["emoji"]
                    }
                },
                "filter": {
                    "english_stop": {
                        "type": "stop",
                        "stopwords": "_english_",
                    }
                },
                "char_filter": {
                    "emoji": {
                        "type": "mapping",
                        "mappings": [
                            ":) => happy",
                            ":( => sad",
                        ]
                    }
                }
            }
        }
    }
    ```


# Define and use multi-fields

- Allow you to index the same field as multiple types (or, with different analyzers)

```
PUT sample-2
{
    "mappings": {
        "properties": {
            "field_1": {
                "type" : "keyword",
                "fields": {
                    "standard": {
                        "type": "text"
                    },
                    "simple": {
                        "type": "text",
                        "analyzer": "simple"
                    },
                    "english": {
                        "type" : "text",
                        "analyzer": "english"
                    }
                }
            }
        }
    }
}
```

# Configure index to maintain nested array relationships

- Create an index with default nested array
    ```
    PUT nested_array-1/_doc/1
    {
        "group": "LA Instructors",
        "instructors": [
            {
                "firstname": "Myles",
                "lastname": "Young",
            },
            {
                "firstname": "Landon",
                "lastname": "Fowler",
            }
        ]
    }
    ```
- Problem with this default:
    - This search shuoldn't return anything:
        ```
        GET nested_array-1/_search
        {
            "query": {
                "bool": {
                    "must": [
                        "term": {
                            "instructors.firstname.keyword": {
                                "value": "Myles"
                            }
                        },
                        "term": {
                            "instructors.lastname.keyword": {
                                "value": "Fowler"
                            }
                        }
                    ]
                }
            }
        }
        ```
    - But, it'll return the document
    - Within the document, there's no "sense of ownership" within the sub-objects.

- There's a special nested type that retains this information
    ```
    PUT nested_array-2
    {
        "mappings": {
            "properties": {
                "instructors": {
                    "type": "nested"
                }
            }
        }
    }
    ```

- Now, we can query with:
    ```
    GET nested_array-2/_search
    {
        "query": {
            "nested": {
                "path": "instructors",
                "query": {
                    "bool": {
                        "must": [
                            "term": {
                                "instructors.firstname.keyword": {
                                    "value": "Myles"
                                }
                            },
                            "term": {
                                "instructors.lastname.keyword": {
                                    "value": "Fowler"
                                }
                            }
                        ]
                    }
                },
                "inner_hist": {
                    "highlight": {
                        "fields": {
                            "instructors.firstname.keyword": {},
                            "instructors.lastname.keyword": {},
                        }
                    }
                }
            }
        }
    }
    ```

# Creating an index with a parent/child relationship

- Data in Elasticsearch is denormalized (intentionally)
- How do we represent relationships in this setup?
- Join data type!
    - use it very sparingly.


```
PUT parent_child-1
{
    "mappings": {
        "properties": {
            "qa": {
                "type": "join",
                "relations": {
                    "question": "answer"
                }
            }
        }
    }
}

# Recommendation: when using parent-child relationships,
# don't use randomly-generated IDs.
PUT parent_child-1/_doc/1
{
    "text": "Which node type in Elasticsearch stores data?",
    "qa": {
        "name": "question"
    }
}

# Routing: Link to parent (the parent's ID). Keep them in the same shard (required)
PUT parent_child-1/_doc/2?routing=1
{
    "text": "Data node",
    "qa": {
        "name": "answer",
        # ID of parent document
        "parent": 1
    }
}
```

# Routing indexes to shards

- Example index:
```
PUT sample-1
{
    "settings": {
        "number_of_replicas": 0,
        "number_of_shards": 2
    }
}
```

- Check sharding with
    ```
    GET _cat/shards/sample-1
    ```

- Override the default shard locations with
    ```
    PUT sample-1/_settings
    {
        "index.routing.allocation.exclude._name": "data-2"
    }
    ```
    - Will only follow these rules if they'll leave the index in a green state.

- Can make "transient" changes (useful for, for example, offloading traffic/data so you can
    make hardware changes).

# Shard allocation awareness and forced awareness for an index

- Suppose you've tagged each of your nodes with what zone they're in. How can you replicate across zones?
    ```
    PUT _cluster/settings
    {
        "persistent": {
            "cluster.routing.allocation.awareness.attributes": "zone"
        }
    }
    ```

- If a zone falls over and your cluster is more than 50% full, how do you keep from killing the second zone?
    - Turn down awareness
    ```
    PUT _cluster/settings
    {
        "persistent": {
            "cluster.routing.allocation.awareness.attributes": "zone",
            "cluster.routing.allocation.awareness.force.zone.values": "1,2"
        }
    }
    ```
    - This makes your shards go yellow until you bring up a new zone.

# Diagnosing shard issues and repairing cluster health

- Cluster application explain API
    ```
    GET _cluster/allocation/explain
    ```

# Back up and restore a cluster or indices

- requires a shared filesystem.
- In `config/elasticsearch.yaml`
    ``` yaml
    path.repo: /home/elastic/snapshots
    ```
- Then, create a repo with
    ```
    PUT _snapshot/my_repo
    {
        "type": "fs",
        "settings": {
            "location": "/home/elastic/snapshots"
        }
    }
    ```
- Create a backup with:
    ```
    # Asynchronous call by default
    PUT _snapshot/my_repo/bank_snapshot-1?wait_for_completion=true
    {
        "indices": "bank"
    }
    ```
- Create a disaster with:
    ```
    DELETE bank
    ```
- Restore with
    ```
    POST _snapshot/my_repo/bank_snapshot-1/_restore
    {
        "indices": "bank"
    }
    ```

# Configure hot/warm architecture

- Create an index for recent logs (`recent_logs`) and old logs (`old_logs`)
- Route new index to the hot nodes
    ```
    PUT recent_logs/_settings
    {
        "index.routing.allocation.require.temp": "hot"
    }

    PUT old_logs/_settings
    {
        "index.routing.allocation.require.temp": "warm"
    }
    ```

# Configure cluster for cross-cluster search

- On cluster 1
    ```
    PUT _cluster/settings
    {
        "persistent": {
            "cluster": {
                "remote": {
                    "c2": {
                        "seeds": ["<ip of cluster>:9300"]
                    }
                }
            }
        }
    }
    ```

# Query

- Results are limited to 10,000 results by default. You need to use the scrolling API to get more.
- Basic match query
    ```
    GET _search
    {
        "query": {
            "match": {
                "text_entry_field": "search terms"
            }
        }
    }
    ```
    -  Searches across intersection of the tokens. So, all words OR'd together.

- Match a phrase
    ```
    GET _search
    {
        "query": {
            "match_phrase": {
                "field_name": "search phrase"
            }
        }
    }
    ```

- Multimatch query: Search across multiple fields
    ```
    GET _search
    {
        "query": {
            "multi_match": {
                "query": "Crime",
                "fields": [
                    "text_entry", "relatedContent.og:description"
                ]
            }
        }
    }
    ```

- Query query: Complex queries using a query DSL
    ```
    GET _search
    {
        "query": {
            "query_string": {
                "defualt_field": "field_name",
                "query": "romeo OR juliet"
            }
        }
    }
    ```

# Term queries

Non-analyzed query examples

- Example searching against a field containing a non-analyzed literal value
    ```
    GET _search
    {
        "query": {
            "term": {
                "speaker": {
                    "value": "ROMEO"
                }
            }
        }
    }
    ```

- Terms: Mulitmatch for terms
    ```
    GET _search
    {
        "query": {
            "terms": {
                "speaker": ["ROMEO", "JULIET"]
            }
        }
    }
    ```

- Range query
    ```
    GET _search
    {
        "query": {
            "range": {
                "age": {
                    "gte": 40,
                    "lt": 50
                }
            }
        }
    }
    ```

- Term-level regex query
    ```
    GET _search
    {
        "query": {
            "wildcard": {
                "city.keyword": {
                    "value": "*ville"
                }
            }
        }
    }
    ```

    ```
    GET _search
    {
        "query": {
            "regexp": {
                "email.keyword": ".*@xurban\\.com"
            }
        }
    }
    ```

# Combining queries

- 
    ```
    GET _search
    {
        "query": {
            "bool": {
                "must": [
                    {
                        "term": {
                            "gender.keyword": {
                                "_name": "gender",
                                "value": "F"
                            }
                        }
                    }
                ],
                "should: [
                    {
                        "term": {
                            "lastname.keyword": {
                                "_name": "lastname_1",
                                "value": "Meyers"
                            }
                        }
                    },
                    {
                        "term": {
                            "lastname.keyword": {
                                "_name": "lastname_2",
                                "value": "Owens"
                            }
                        }
                    }
                ],
                "must_not": [
                    {
                        "term": {
                            "state.keyword": {
                                "_name": "state",
                                "value": "RI"
                            }
                        }
                    }
                ],
                "minimum_should_match": 1,
                "filter": {
                    // Reduce dataset without affecting doc score.
                    "term": {
                        "city.keyword": "Jacksonburg"
                    }
                }
            }
        }
    }
    ```

# Highlight search terms in response

- 
    ```
    GET _search
    {
        "highlight": {
            "pre_tags": ["<tag1>"],
            "post_tags: ["</tag1>"],
            "fields": {
                "text_entry": {}
            }
        }
        "query": {
            "match": {
                "text_entry": "life"
            }
        }
    }
    ```

# Sort results

- 
    ```
    GET bank/_search
    {
        "sort": [
            {
                "account_number": {
                    "order": "asc"
                }
            }
        ]
    }
    ```

# Pagination

- Page size
    ```
    GET bank/_search?size=20
    ```
- Get second page
    ```
    GET bank/_search?size=20&from=20
    ```

# Scroll API

- By default, ES will stop returning results once it hits 1000 docs.
- scrolling will give you the version of the document that existed when you opened the scroll!
- `scroll` is how long you want the context to remain open
    ```
    GET _search?scroll=10m&size=1000
    ```

- Get next set from scroll
    ```
    GET _search/scroll
    {
        "scroll": "10m",
        "scroll_id": "_scroll_id field from response of initial scroll query"
    }
    ```

- Close the query once you're done.
    ```
    DELETE _search/scroll
    {
        "scroll_id": "_scroll_id"
    }
    ```

- Best practice: sort by doc id
    ```
    GET _search?scroll=10m&size=1000
    {
        "sort": [
            {
                "_doc": {
                    "order": "asc"
                }
            }
        ]
    }
    ```

- Close all scrolls
    ```
    DELETE _search/scroll/_all
    ```

- Sliced scroll
    - Breaks scroll across parallel nodes
    ```
    GET _search?scroll=10m
    {
        "slice": {
            "id": 0,
            "max": 2
        },
        "sort": [
            {
                "_doc": {
                    "order": "asc"
                }
            }
        ]
    }

    GET _search?scroll=10m
    {
        "slice": {
            "id": 1,
            "max": 2
        },
        "sort": [
            {
                "_doc": {
                    "order": "asc"
                }
            }
        ]
    }
    ```

# Fuzzy queries

- Analyzed fuzzy query
    ```
    GET _search
    {
        "query": {
            "match": {
                "text_entry": {
                    "query": "shae",
                    "fuzzieness": 1,
                    "transpositions": true  // Allow characters to be swapped?
                }
            }
        }
    }
    ```

# Search template

- Let's you add user input to your templates
    ```
    GET _search/template
    {
        "source": {
            "query": {
                "bool": {
                    "must": [
                        {
                            "wildcard": {
                                "firstname.keyword": {
                                    // Use parameter. If it's not set, use `*`
                                    "value": "{{first}}{{^first}}*{{/first}}"
                                }
                            }
                        },
                        {
                            "wildcard": {
                                "lastname.keyword": {
                                    "value": "{{last}}{{^last}}*{{/last}}"
                                }
                            }
                        }
                    ]
                }
            }
        },
        "params": {
            "first": "*"
        }
    }
    ```

- Save a scripted query
    ```
    POST _scripts/account_lookup_by_name
    {
        "script": {
            "lang": "mustache",
            "source": {
                ...
            }
        }
    }
    ```

- Run a scripted query
    ```
    GET _search/template
    {
        "id": "account_lookup_by_name",
        "params": {
            "first": "*"
        }
    }
    ```

# Query across multiple clusters

- Must have cross-cluster searches enabled by admin.
- Ask a remote cluster a question
    ```
    GET <cluster_id>:bank/_search
    {
        "query": {
            "term": {
                "firstname": {
                    "value": "Amber"
                }
            }
        }
    }
    ```

# Metrics and bucket aggregations

- Three main types of aggs: Bucket, Metric, Pipeline
- Aggregating on Keywords is good. Aggregating on text is extremely expensive.
    - Solution for text: Index as a multifield, with one of the fields being keyword type.

- Example: Unique count
    ```
        GET logs/_search 
        {
            "size": 0, # Hide the actual search results. Only show the agg.
            "aggs": {
                "name_of_agg": {
                    "cardinality": {
                        "field": "clientip.keyword"
                    }
                }
            }
        }
    ```

- Metrics aggregation:
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            "the_sum": {
                "sum":{
                    "field": "bytes"
                }
            },
            "the_avg": {
                "avg":{
                    "field": "bytes"
                }
            }
        }
    }
    ```.
- Terms aggregation: Count total documents per value
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            "response_codes": {
                "terms": {
                    "field": "response.keyword",
                    "size": 10  # Number of buckets.
                }
            }
        }
    }
    ```
- Time series
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            "requests_per_day": {
                "date_histogram": {
                    "field": "@timestamp",
                    "calendar_interval": "day"
                }
            }
        }
    }
    ```

# Aggregations with sub-aggregations

- Do a metric agg inside a bucket agg
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            "extension": {
                "terms": {
                    field: "extension.keyword",
                    "size": 10,
                    "order": {
                        "sum_of_bytes": "desc"
                    }
                },
                "aggs": {
                    "sum_of_bytes": {
                        "sum": {
                            "field": "bytes"
                        }
                    }
                }
            }
        }
    }
    ```

# Pipeline aggregations

- Sibling aggregations
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            "extensions": {
                "terms": {
                    "field": "extension.keyword",
                    "size": 10,
                    "order": {
                        "sum_of_bytes": "desc"
                    }
                },
                "aggs": {
                    "sum_of_bytes": {
                        "sum": {
                            "field": "bytes"
                        }
                    }
                }
            },
            "total": {
                # Get a sum of the `sum_of_bytes` agg.
                "sum_bucket": {
                    "bucket_path": "extensions>sum_of_bytes"
                }
            }
        }
    }
    ```
- Parent aggregation
    ```
    GET logs/_search
    {
        "size": 0,
        "aggs": {
            # For each hour, what's the average bytes per second?
            "per_hour": {
                "date_histogram": {
                    "field": "@timestamp",
                    "calendar_interval": "hour"
                },
                "aggs": {
                    "sum_of_bytes": {
                        "sum": {
                            "field": "bytes"
                        }
                    },
                    "cumulative_sum_of_bytes": {
                        "sumulative_sum": {
                            "buckets_path": "sum_of_bytes"
                        }
                    },
                    "bytes_per_second": {
                        "derivative": {
                            "buckets_path": "cumulative_sum_of_bytes",
                            "unit": "second"
                        }
                    }
                }
            }
        }
    }
    ```