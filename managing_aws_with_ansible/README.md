
# Ansible + AWS

To use Ansible's AWS modules, you'll need to install Boto3: `pip install boto3`

Most modules target the AWS command line tool, so it makes sense to put `localhost` in your inventory.

## Static vs dynamic inventory

- Dynamic inventory is a script that will build inventory on the fly.
- Can also use the `add_host` module in Ansible.

## Configuring EC2 access
- Configure a key pair.
- Can check access to servers with `ansible <group> -m ping -u cloud_user`
- Configure agent forwarding by adding the following to `~/.ssh/config`
    ```
    Host *.amazonaws.com
        ForwardAgent yes
    ```
- `ssh-agent` can be used to manage public keys more easily.
    - E.g., `ssh-add /path/to/aws/key.pem`
    - Use `ssh-add -l` to see what keys have been added.
    - Use `ss-add -D` to drop all keys from agent.

## Creating an IAM user for use with Ansible

- When creating the IAM user, select "Programmatic access"
- Copy the secret access key - it'll only be available once
- Better permission assignments
    - Use principle of least privilege.
    - Only a few policies matter for this course: EC2 Full Access, S3 full access, IAM full access, VPC full access

## Ansible Vault

- How do you encrypt your AWS key IDs?
- `ansible-vault encrypt keys.yaml`
    - Will request a password
- Now, run playbooks with `--ask-vault-pass` to use the values.

## Provisioning EC2 instances

- Module: `ec2`
- One of the params is `key_name`. Tells AWS what keypair to use to allow you to log in.
- `wait`: Pause until AWS says the node is up and running.
    - Can add a timeout.
- `count`: How many _new_ instances to create?
- `exact_count`: How many shuold exist when the playbook is done?
    - Used in conjunction iwth `count_tag` to determine how many nodes exist.
- `state` can be used to shut down nodes.

- `add_host` module can be used to dynamically add hosts to a group.

## EC2 Facts

- `ec2_instance_facts` module
    - `filters` can be used to, e.g., filter by tag.
    - `register` can then be used at the task to save the results for later use.
- `ec2_metadata_facts`
    - Gets the facts on a specific EC2 instance
    - More details than `ec2_instance_facts`
    - No parameters
    - It actaully connects to the host and checks the host itself for data (as opposed to contacting the AWS console).
    