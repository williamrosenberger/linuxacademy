# Kubernetes Lab - Learn by doing

## Creating the cluster

Overview of process:
1. Install docker on all nodes
2. Install kubeadm, kubelet, kubectl on all nodes.
3. Create the cluster on the master node
4. Join the workers to the master
5. Set up cluster networking

Notes:
- Install docker 
    - Standard install process.
- Install kubelet, kubeadm, and kubectl
- Activate the cluster mater with `sudo kubeadm init --pod-network-cidr=10.244.0.0/16`
- Create your kubeconfig directory
    ``` bash
        mkdir -p $HOME/.kube
        sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
        sudo chown $(id -u):$(id -g) $HOME/.kube/config
    ```
    - If you're able to talk to the k8s master, then running `kubectl version` will display the server's version number (not just the client's)
- The `kubeadm init` that was run above will print a `kubeadm join` command. Run this on all member nodes.
- At this point, `kubectl get nodes` on the master node should report `NotReady` for all nodes. To fix this, you need to set up networking.
- Enable networking
    ```
        $ echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.conf
        $ sudo sysctl -p
        $ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
    ```

## Deploying a simple service

Create a service that provides a list of products.

- Deployed `simple_deployment.yaml` with `kubectl apply -f <yaml file>`
- Deployed `simple_service.yaml`
- `kubectl get pods` shows the pods are running!
- Can use `kubectl exec busybox -- curl -s store-products` to query the service from within the cluster.
    - That's... really epic. This is amazing!

## Deploying an application to K8s

The application can be found in the [kubernetes_learn_by_doing/robot-shop](/kubernetes_learn_by_doing/robot-shop) submodule.

1. Deployment
    - Create a namespace: `kubectl create namespace robot-shop`
    - Deploy the app: `kubectl -n robot-shop create -f ~/robot-shop/K8s/descriptors/`
        - `-n robot-shop` puts all components in the namespace.
        - Note that they're running _everything_ in the `descriptors/` directory. I didn't realize you could use `-f` with a directory!
        - I tried running `kubectl get pods` to check that everything was running. But, things were launched within a namespace, so nothing showed up. Running `kubectl get pods -n robot-shop` worked.
1. Scaling
    - Before scaling, `kubectl get pods -n robot-shop` gave:
        ```
            NAME                         READY   STATUS              RESTARTS   AGE
            cart-59b7dfbdcc-mpm5d        0/1     ContainerCreating   0          26s
            catalogue-796444cf68-2f2c6   0/1     ContainerCreating   0          26s
            dispatch-76cf85ddc6-mnsbj    0/1     ContainerCreating   0          26s
            mongodb-5969679ff7-9nj9q     0/1     ContainerCreating   0          26s
            mysql-7958997695-qbmdv       0/1     ContainerCreating   0          26s
            payment-6fd7c8ff75-dz9dk     0/1     ContainerCreating   0          25s
            rabbitmq-6ddb6485bd-659hd    0/1     ContainerCreating   0          25s
            ratings-5fd67676b4-4zssh     0/1     ContainerCreating   0          25s
            redis-dff4dbc55-2qvb7        0/1     ContainerCreating   0          25s
            shipping-8695b6c8-wt2pv      0/1     ContainerCreating   0          25s
            user-59d45d4db6-m6cl7        0/1     ContainerCreating   0          25s
            web-757b94795b-xqshh         0/1     ContainerCreating   0          25s
        ```
        - Note that there's only one MongoDB pod.
    - Changed `kubernetes_learn_by_doing/robot-shop/K8s/descriptors/mongodb-deployment.yaml` to `replicas: 2`.
    - Tried redeploying by just using the same command I ran before. That failed because all the resources already exist. The proper command for updating the deployment is: `kubectl apply -n robot-shop -f ~/robot-shop/K8s/descriptors/`
        - Got a bunch of warnings saying "Warning: kubectl apply should be used on resource created by either kubectl create --save-config or kubectl apply"
        - I tried editing the mongodb config file again and tried re-deploying. The warnings were gone. So, I guess the initial create should be run with `--save-config`.
        - Class lab recommends using `kubectl edit deployment mongodb -n robot-shop`

## Useful commands

- `kubectl get nodes`
- `kubectl get pods --all-namespaces`
- `kubectl get namespaces`
- `kubectl get pods`
    - Only gets things in the `default` namespace
- `kubectl get pods --all-namespaces -o wide`
    - Includes IP addresses.
- `kubectl get deployments`
- Can forward ports using:
    - `kubectl port-forward <pod-name> 8081:80`
        - forwards host's `:8081` to pod's `:80`.
- Can execute commands within a pod with:
    - `kubectl exec -it <pod_name> -- nginx -v`
- Expose a deployment with:
    - `kubectl expose deployment <deployment-name> --port 80 --type NodePort`
    - `kubectl get services` shows that this creates a new service that forwards pod's `:80` to host's `:31743`
    - Can `curl -I localhost:31743`.

## DNS in Kubernetes

- DNS names are created by services.
    - Suppose you created a deployment with `kubectl run nginx --image=nginx`
    - DNS name `nginx` will be available within the K8S network once you run: `kubectl expose deployment nginx --port 80 --type NodePort`
    - That is, you'll be able `exec` into another running pod and lookup the `nginx` server - e.g., `kubectl exec busybox -- nslookup nginx`

## Tainting workers

- As described [here](https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/#taint-nodes-by-condition), node affiniity "is a property of pods that attracts them to a set of nodes".
- Taints are the opposite - they allow a node to repel a set of pods.
- Taint with: `kubectl taint node <node_name> node-type=prod:NoSchedule`
    - `node-type=prod:NoSchedule` is the taint.
    - It's a filter of the form `key=value:effect`.
    - This taint means: "Do not schedule pods on nodes with node-type `prod`".
        - Note: Tolerations can be applied to pods to override taints.
- You can see what nodes each pod is running on with `kubectl get pods -o wide`
- You can see the full YAML definition for a pod with `kubectl get pods <pod_name> -o yaml`

## Version rollouts

Example of a Deployment:
``` yaml
 apiVersion: apps/v1
 kind: Deployment
 metadata:
   name: kubeserve
 spec:
   replicas: 3
   selector:
     matchLabels:
       app: kubeserve
   template:
     metadata:
       name: kubeserve
       labels:
         app: kubeserve
     spec:
       containers:
       - image: linuxacademycontent/kubeserve:v1
         name: app
```

- Get the status of a rollout with `kubectl rollout status deployments kubeserve`
- Describe a deployment's current version with: `kubectl describe deployment kubeserve`
- Scale a deployment with `kubectl scale deployment kubeserve --replicas=5`
- Create a service so that you can access the deployment:
    - `kubectl expose deployment kubeserve --port 80 --target-port 80 --type NodePort`
- Change a deployment's image with: `kubectl set image deployments/kubeserve app=linuxacademycontent/kubeserve:v2 --v 6`
- View ongoing versions with `kubectl get replicasets`
- View deployment history with `kubectl rollout history deployment kubeserve`

## Persistant storage

Example of a persistant volume:

``` yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: redis-pv
spec:
  storageClassName: ""
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

Example of a persistant volume claim:

``` yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: redisdb-pvc
spec:
  storageClassName: ""
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

Exampe of a pod using a persistant volume:

``` yaml
apiVersion: v1
kind: Pod
metadata:
  name: redispod
spec:
  containers:
  - image: redis
    name: redisdb
    volumeMounts:
    - name: redis-data
      mountPath: /data
    ports:
    - containerPort: 6379
      protocol: TCP
  volumes:
  - name: redis-data
    persistentVolumeClaim:
      claimName: redisdb-pvc
```

## ClusterRole

Pods cannot access persistant volumes by default. `ClusterRole`s  and `ClusterRoleBinding`s can be used to give access based on user, service account, or group.

Create a cluster role with: `kubectl create clusterrole pv-reader --verb=get,list --resource=persistentvolumes`

Create a cluster role binding with: `kubectl create clusterrolebinding pv-test --clusterrole=pv-reader --serviceaccount=web:default`

The example pod to which we'll try to apply the role:
``` yaml
 apiVersion: v1
 kind: Pod
 metadata:
   name: curlpod
   namespace: web  # Note: This is listed in the serviceacount above.
 spec:
   containers:
   - image: tutum/curl
     command: ["sleep", "9999999"]
     name: main
   - image: linuxacademycontent/kubectl-proxy
     name: proxy
   restartPolicy: Always
```

This pod can access the PV endpoints! Try:
```
$ kubectl exec -it curlpod -n web -- sh
$ curl localhost:8001/api/v1/persistentvolumes
```

## Smoke testing

### Test encryption for secrets

Create a secret with: `kubectl create secret generic kubernetes-the-hard-way --from-literal="mykey=mydata"`

View the secret value with:
```
sudo ETCDCTL_API=3 etcdctl get \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/etcd/ca.pem \
  --cert=/etc/etcd/kubernetes.pem \
  --key=/etc/etcd/kubernetes-key.pem\
  /registry/secrets/default/kubernetes-the-hard-way | hexdump -C
```

## Upgrading

Check current version with `kubectl version --short`

1. Upgrade `kubeadm` on the master node
  - On master, run:
    ```
    $ sudo apt-mark unhold kubeadm
    $ sudo apt install -y kubeadm=1.16.6-00
    ```
1. Upgrade control plane component
  - On master, run:
    ```
    $ sudo kubeadm upgrade plan
    $ sudo kubeadm upgrade apply v1.16.6
    ```
1. Upgrade kubelet and kubectl on master
  - On master, run:
    ```
    $ sudo apt-mark unhold kubelet
    $ sudo apt install -y kubelet=1.16.6-00
    $ sudo apt-mark unhold kubectl
    $ sudo apt install -y kubectl=1.16.6-00
    ```
1. Upgrade kubelet on all the workers
  - On each worker, run:
    ```
    $ sudo apt-mark unhold kubelet
    $ sudo apt install -y kubelet=1.16.6-00
    ```

## Getting logs

1. Identify problematic pods
  - `kubectl get pods --all-namespaces`
  - In particular, notice the `STATUS` and `READY` columns.
1. Get logs
  - `kubectl logs <pod_name> -n <namespace_name>`
1. Describe pod status
  - If a pod is stuck in a non-running status, `kubectl describe pod <pod_name>` frequently
    contains really useful information.

## Monitoring with prometheus

### Service discoverty for prometheus

`prometheus-config-map.yml`:

Defines 2 jobs that define how to launch Prometheus

This file was created by the lab
``` yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: prometheus-server-conf
  labels:
    name: prometheus-server-conf
  namespace: monitoring
data:
  prometheus.yml: |-
    global:
      scrape_interval: 5s
      evaluation_interval: 5s

    scrape_configs:
      - job_name: 'kubernetes-apiservers'

        # Service discovert configuration
        kubernetes_sd_configs:
        - role: endpoints
        scheme: https

        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        relabel_configs:
        - source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_service_name, __meta_kubernetes_endpoint_port_name]
          action: keep
          regex: default;kubernetes;https

      - job_name: 'kubernetes-cadvisor'

        scheme: https

        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
        bearer_token_file: /var/run/secrets/kubernetes.io/serviceaccount/token

        kubernetes_sd_configs:
        - role: node

        relabel_configs:
        - action: labelmap
          regex: __meta_kubernetes_node_label_(.+)
        - target_label: __address__
          replacement: kubernetes.default.svc:443
        - source_labels: [__meta_kubernetes_node_name]
          regex: (.+)
          target_label: __metrics_path__
          replacement: /api/v1/nodes/${1}/proxy/metrics/cadvisor
```

Once that file is changed, apply it, delete the prometheus pod, and wait for it to come back up.

### Setting up alert rules

Change `prometheus-rules-config-map.yml` to the following:

``` yaml
apiVersion: v1
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: prometheus-rules-conf
  namespace: monitoring
data:
  redis_rules.yml: |
    groups:
    - name: redis_rules
      rules:
      - record: redis:command_call_duration_seconds_count:rate2m
        expr: sum(irate(redis_command_call_duration_seconds_count[2m])) by (cmd, environment)
      - record: redis:total_requests:rate2m
        expr: rate(redis_commands_processed_total[2m])
  redis_alerts.yml: |
    groups:
    - name: redis_alerts
      rules:
      - alert: RedisServerDown
        expr: redis_up{app="media-redis"} == 0
        for: 10m
        labels:
          severity: critical
        annotations:
          summary: Redis Server {{ $labels.instance }} is down!
      - alert: RedisServerGone
        expr:  absent(redis_up{app="media-redis"})
        for: 1m
        labels:
          severity: critical
        annotations:
          summary: No Redis servers are reporting!
```

This defines the monitoring rules for prometheus.

## Repairing failed pods

- `kubectl get pods --all-namespaces` showed that `nginx` pods in the `web` namespace couldn't pull
  their image (`STATUS: ImagePullBackOff`)
- `kubectl describe pod nginx-856876659f-hgx8l -n web` shows that the pods are trying to pull an image called `nginx:191`
  - Does not exist in DockerHub. Correct image should be `nginx:1.9.1`
- Retrieved current deployment of the pod with `kubectl get pod nginx-856876659f-hgx8l -n web -o yaml > template.yaml`
- Updated the image tag and applied with `kubectl apply -f template.yaml`
- This correctly fixed just the one pod. That was stupid. These pods are of course driven by a deployment.
  `kubectl get deployments --all-namespaces` shows that the sick deployment is `nginx`
- `kubectl get deployment nginx -n web -o yaml > template.yaml` and fixed the image tag. Applied the change.
- All pods are now running!

## Creating a CA

Create the CA
```
$ cat > ca-config.json << EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

$ cat > ca-csr.json << EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "CA",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert -initca ca-csr.json | cfssljson -bare ca
```

Generate client certs

``` bash
$ cat > admin-csr.json << EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:masters",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  admin-csr.json | cfssljson -bare admin
```

Kubelet client certs:

``` bash
$ cat > worker0.mylabserver.com-csr.json << EOF
{
  "CN": "system:node:worker0.mylabserver.com",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=172.34.1.0,worker0.mylabserver.com \
  -profile=kubernetes \
  worker0.mylabserver.com-csr.json | cfssljson -bare worker0.mylabserver.com

$ cat > worker1.mylabserver.com-csr.json << EOF
{
  "CN": "system:node:worker1.mylabserver.com",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=172.34.1.1,worker1.mylabserver.com \
  -profile=kubernetes \
  worker1.mylabserver.com-csr.json | cfssljson -bare worker1.mylabserver.com
```

Kube controller manager client cert
``` bash
$ cat > kube-controller-manager-csr.json << EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-controller-manager",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager
``` 

Proxy client cert:

``` bash
$ cat > kube-proxy-csr.json << EOF
{
  "CN": "system:kube-proxy",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:node-proxier",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-proxy-csr.json | cfssljson -bare kube-proxy
``` 

Kube scheduler
``` bash
$ cat > kube-scheduler-csr.json << EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "system:kube-scheduler",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler
```

Generate API server cert

``` bash
$ cat > kubernetes-csr.json << EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=10.32.0.1,172.34.0.0,controller0.mylabserver.com,172.34.0.1,controller1.mylabserver.com,172.34.2.0,kubernetes.mylabserver.com,127.0.0.1,localhost,kubernetes.default \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes
```

Generate a service account key pair
``` bash
$ cat > service-account-csr.json << EOF
{
  "CN": "service-accounts",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Portland",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Oregon"
    }
  ]
}
EOF

$ cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  service-account-csr.json | cfssljson -bare service-account
```

## Generating kubeconfigs for a new cluster

Kubeconfigs for all workers
``` bash
KUBERNETES_PUBLIC_ADDRESS=172.34.2.0

for instance in worker0.mylabserver.com worker1.mylabserver.com; do
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-credentials system:node:${instance} \
    --client-certificate=${instance}.pem \
    --client-key=${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig

  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done
```

Config for kube-proxy
``` bash
KUBERNETES_PUBLIC_ADDRESS=172.34.2.0

{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-credentials system:kube-proxy \
    --client-certificate=kube-proxy.pem \
    --client-key=kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
}
```

Kubeconf for controller-manager

``` bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-credentials system:kube-controller-manager \
    --client-certificate=kube-controller-manager.pem \
    --client-key=kube-controller-manager-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-controller-manager \
    --kubeconfig=kube-controller-manager.kubeconfig

  kubectl config use-context default --kubeconfig=kube-controller-manager.kubeconfig
}
```

Config for kube-scheduler
``` bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-credentials system:kube-scheduler \
    --client-certificate=kube-scheduler.pem \
    --client-key=kube-scheduler-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-scheduler \
    --kubeconfig=kube-scheduler.kubeconfig

  kubectl config use-context default --kubeconfig=kube-scheduler.kubeconfig
}
```

Create kubeconfig admin conf

``` bash
{
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://127.0.0.1:6443 \
    --kubeconfig=admin.kubeconfig

  kubectl config set-credentials admin \
    --client-certificate=admin.pem \
    --client-key=admin-key.pem \
    --embed-certs=true \
    --kubeconfig=admin.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=admin \
    --kubeconfig=admin.kubeconfig

  kubectl config use-context default --kubeconfig=admin.kubeconfig
}
```

## Generating data encryption config

Create the config with:

``` bash
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)

cat > encryption-config.yaml << EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
```

Add that config to all controller servers (in their home directory)

## Bootstrap workers

- Install packages: `sudo apt-get -y install socat conntrack ipset`
- Download required binaries:
  ``` bash
  wget -q --show-progress --https-only --timestamping \
    https://github.com/kubernetes-incubator/cri-tools/releases/download/v1.0.0-beta.0/crictl-v1.0.0-beta.0-linux-amd64.tar.gz \
    https://storage.googleapis.com/kubernetes-the-hard-way/runsc \
    https://github.com/opencontainers/runc/releases/download/v1.0.0-rc5/runc.amd64 \
    https://github.com/containernetworking/plugins/releases/download/v0.6.0/cni-plugins-amd64-v0.6.0.tgz \
    https://github.com/containerd/containerd/releases/download/v1.1.0/containerd-1.1.0.linux-amd64.tar.gz \
    https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kubectl \
    https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kube-proxy \
    https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kubelet

  sudo mkdir -p \
    /etc/cni/net.d \
    /opt/cni/bin \
    /var/lib/kubelet \
    /var/lib/kube-proxy \
    /var/lib/kubernetes \
    /var/run/kubernetes

  chmod +x kubectl kube-proxy kubelet runc.amd64 runsc

  sudo mv runc.amd64 runc

  sudo mv kubectl kube-proxy kubelet runc runsc /usr/local/bin/

  sudo tar -xvf crictl-v1.0.0-beta.0-linux-amd64.tar.gz -C /usr/local/bin/

  sudo tar -xvf cni-plugins-amd64-v0.6.0.tgz -C /opt/cni/bin/

  sudo tar -xvf containerd-1.1.0.linux-amd64.tar.gz -C /
  ```

- Configure `containerd`
  ``` bash
  $ sudo mkdir -p /etc/containerd/
  $ cat << EOF | sudo tee /etc/containerd/config.toml
[plugins]
  [plugins.cri.containerd]
    snapshotter = "overlayfs"
    [plugins.cri.containerd.default_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runc"
      runtime_root = ""
    [plugins.cri.containerd.untrusted_workload_runtime]
      runtime_type = "io.containerd.runtime.v1.linux"
      runtime_engine = "/usr/local/bin/runsc"
      runtime_root = "/run/containerd/runsc"
EOF

  # Create unit file for containerd
  $ cat << EOF | sudo tee /etc/systemd/system/containerd.service
[Unit]
Description=containerd container runtime
Documentation=https://containerd.io
After=network.target

[Service]
ExecStartPre=/sbin/modprobe overlay
ExecStart=/bin/containerd
Restart=always
RestartSec=5
Delegate=yes
KillMode=process
OOMScoreAdjust=-999
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity

[Install]
WantedBy=multi-user.target
EOF
  ```

- Configure kubelet
  - Move configs into place
    ``` bash
    HOSTNAME=<worker1 or worker0, depending on the server>.mylabserver.com
    sudo mv ${HOSTNAME}-key.pem ${HOSTNAME}.pem /var/lib/kubelet/
    sudo mv ${HOSTNAME}.kubeconfig /var/lib/kubelet/kubeconfig
    sudo mv ca.pem /var/lib/kubernetes/
    ```
  - Create full kubelet config file
    ``` bash
    cat << EOF | sudo tee /var/lib/kubelet/kubelet-config.yaml
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  anonymous:
    enabled: false
  webhook:
    enabled: true
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "10.32.0.10"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/${HOSTNAME}.pem"
tlsPrivateKeyFile: "/var/lib/kubelet/${HOSTNAME}-key.pem"
EOF
    ```
  - Create unit file for kubelet
    ``` bash
    cat << EOF | sudo tee /etc/systemd/system/kubelet.service
[Unit]
Description=Kubernetes Kubelet
Documentation=https://github.com/kubernetes/kubernetes
After=containerd.service
Requires=containerd.service

[Service]
ExecStart=/usr/local/bin/kubelet \\
  --config=/var/lib/kubelet/kubelet-config.yaml \\
  --container-runtime=remote \\
  --container-runtime-endpoint=unix:///var/run/containerd/containerd.sock \\
  --image-pull-progress-deadline=2m \\
  --kubeconfig=/var/lib/kubelet/kubeconfig \\
  --network-plugin=cni \\
  --register-node=true \\
  --v=2 \\
  --hostname-override=${HOSTNAME} \\
  --allow-privileged=true
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
    ```
- Configure kube-proxy
  - Move config file into place: `sudo mv kube-proxy.kubeconfig /var/lib/kube-proxy/kubeconfig`
  - Create full proxy config file:
    ``` bash
    cat << EOF | sudo tee /var/lib/kube-proxy/kube-proxy-config.yaml
kind: KubeProxyConfiguration
apiVersion: kubeproxy.config.k8s.io/v1alpha1
clientConnection:
  kubeconfig: "/var/lib/kube-proxy/kubeconfig"
mode: "iptables"
clusterCIDR: "10.200.0.0/16"
EOF
    ```
  - Create unit file
    ``` bash
    cat << EOF | sudo tee /etc/systemd/system/kube-proxy.service
[Unit]
Description=Kubernetes Kube Proxy
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-proxy \\
  --config=/var/lib/kube-proxy/kube-proxy-config.yaml
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
    ```
- Start all services
  ``` bash
  sudo systemctl daemon-reload
  sudo systemctl enable containerd kubelet kube-proxy
  sudo systemctl start containerd kubelet kube-proxy
  ```

  - Check that all services are running: `sudo systemctl status containerd kubelet kube-proxy`
  - Check that both nodes are registering with the cluster: `kubectl get nodes --kubeconfig /home/cloud_user/admin.kubeconfig`

## Bootstrapping control plane

- Get binaries
  ``` bash
  $ sudo mkdir -p /etc/kubernetes/config
  $ wget -q --show-progress --https-only --timestamping \
  "https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kube-apiserver" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kube-controller-manager" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kube-scheduler" \
  "https://storage.googleapis.com/kubernetes-release/release/v1.10.2/bin/linux/amd64/kubectl"
  $ chmod +x kube-apiserver kube-controller-manager kube-scheduler kubectl
  $ sudo mv kube-apiserver kube-controller-manager kube-scheduler kubectl /usr/local/bin/
  ```

- Configure kube-apiserver
  - metadata setup
    ``` bash
    $ sudo mkdir -p /var/lib/kubernetes/

    $ sudo cp ca.pem ca-key.pem kubernetes-key.pem kubernetes.pem \
      service-account-key.pem service-account.pem \
      encryption-config.yaml /var/lib/kubernetes/

    $ INTERNAL_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)

    $ ETCD_SERVER_0=<controller 0 private ip>

    $ ETCD_SERVER_1=<controller 1 private ip>
    ```
  - Create unit file
    ``` bash
    cat << EOF | sudo tee /etc/systemd/system/kube-apiserver.service
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=${INTERNAL_IP} \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
  --enable-admission-plugins=Initializers,NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
  --enable-swagger-ui=true \\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers=https://${ETCD_SERVER_0}:2379,https://${ETCD_SERVER_1}:2379 \\
  --event-ttl=1h \\
  --experimental-encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --kubelet-https=true \\
  --runtime-config=api/all \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2 \\
  --kubelet-preferred-address-types=InternalIP,InternalDNS,Hostname,ExternalIP,ExternalDNS
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
    ```

- Configure kube-controller-manager service
  ``` bash
  $ sudo mv kube-controller-manager.kubeconfig /var/lib/kubernetes/

  $ cat << EOF | sudo tee /etc/systemd/system/kube-controller-manager.service
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --address=0.0.0.0 \\
  --cluster-cidr=10.200.0.0/16 \\
  --cluster-name=kubernetes \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
  ```

- Configure `kube-scheduler`
  ``` bash
  $ sudo mv kube-scheduler.kubeconfig /var/lib/kubernetes/

  $ cat << EOF | sudo tee /etc/kubernetes/config/kube-scheduler.yaml
apiVersion: componentconfig/v1alpha1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF

cat << EOF | sudo tee /etc/systemd/system/kube-scheduler.service
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
  ```

- Start services
  - `sudo systemctl daemon-reload`
  - `sudo systemctl enable kube-apiserver kube-controller-manager kube-scheduler`
  - `sudo systemctl start kube-apiserver kube-controller-manager kube-scheduler`
  - Check that everything is working with: `kubectl get componentstatuses --kubeconfig admin.kubeconfig`

- Enable HTTP health checks
  ``` bash
  $ sudo apt-get install -y nginx

  $ cat > kubernetes.default.svc.cluster.local << EOF
server {
  listen      80;
  server_name kubernetes.default.svc.cluster.local;

  location /healthz {
     proxy_pass                    https://127.0.0.1:6443/healthz;
     proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
  }
}
EOF

  $ sudo mv kubernetes.default.svc.cluster.local \
    /etc/nginx/sites-available/kubernetes.default.svc.cluster.local

  $ sudo ln -s /etc/nginx/sites-available/kubernetes.default.svc.cluster.local /etc/nginx/sites-enabled/

  $ sudo systemctl restart nginx

  $ sudo systemctl enable nginx
  ```

  - Verify that health checks are working with: `curl -H "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz`

- Set up RBAC
  - Do the following on only one controller node.
  ``` bash
  $ cat << EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
    kubernetes.io/bootstrapping: rbac-defaults
  name: system:kube-apiserver-to-kubelet
rules:
  - apiGroups:
      - ""
    resources:
      - nodes/proxy
      - nodes/stats
      - nodes/log
      - nodes/spec
      - nodes/metrics
    verbs:
      - "*"
EOF

  $ cat << EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: system:kube-apiserver
  namespace: ""
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:kube-apiserver-to-kubelet
subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: kubernetes
EOF
  ```

## Bootstrapping an etcd cluster

- Install etcd on both conrtol nodes
  ```
  $ wget -q --show-progress --https-only --timestamping \
    "https://github.com/coreos/etcd/releases/download/v3.3.5/etcd-v3.3.5-linux-amd64.tar.gz"
  $ tar -xvf etcd-v3.3.5-linux-amd64.tar.gz
  $ sudo mv etcd-v3.3.5-linux-amd64/etcd* /usr/local/bin/
  ```
- Configure and start etcd on both control nodes
  - Move existing settings into place
    ``` bash
    $ sudo mkdir -p /etc/etcd /var/lib/etcd
    $ sudo cp ca.pem kubernetes-key.pem kubernetes.pem /etc/etcd/
    ```
  - Define env vars
    ``` bash
    ETCD_NAME=<controller-0 or controller-1>
    INTERNAL_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)
    CONTROLLER_0_INTERNAL_IP=<controller 0 private ip>
    CONTROLLER_1_INTERNAL_IP=<controller 1 private ip>
    ```
  - Create unit file
    ``` bash
    $ cat << EOF | sudo tee /etc/systemd/system/etcd.service
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
ExecStart=/usr/local/bin/etcd \\
  --name ${ETCD_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://${INTERNAL_IP}:2380 \\
  --listen-client-urls https://${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster controller-0=https://${CONTROLLER_0_INTERNAL_IP}:2380,controller-1=https://${CONTROLLER_1_INTERNAL_IP}:2380 \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF
    ```
  
  - Start services
    ``` bash
    $ sudo systemctl daemon-reload
    $ sudo systemctl enable etcd
    $ sudo systemctl start etcd
    ```

- Check taht everything is running with:
  ``` bash
  sudo ETCDCTL_API=3 etcdctl member list \
    --endpoints=https://127.0.0.1:2379 \
    --cacert=/etc/etcd/ca.pem \
    --cert=/etc/etcd/kubernetes.pem \
    --key=/etc/etcd/kubernetes-key.pem
  ```

## Cluster networking with Weave Net

- Enable IP forwarding on all worker nodes
  ``` bash
  $ sudo sysctl net.ipv4.conf.all.forwarding=1
  $ echo "net.ipv4.conf.all.forwarding=1" | sudo tee -a /etc/sysctl.conf
  ```
- Install Weave net on the master
  - Apply WeaveNet
    ``` bash
    kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPALLOC_RANGE=10.200.0.0/16"
    ```
    - Check that it's working with `kubectl get pods -n kube-system`
- Check that networking is set up correctly by buidling some pods and trying to connect
  - Deploy nginx
    ``` bash
    cat << EOF | kubectl apply --kubeconfig admin.kubeconfig -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx
spec:
  selector:
    matchLabels:
      run: nginx
  replicas: 2
  template:
    metadata:
      labels:
        run: nginx
    spec:
      containers:
      - name: my-nginx
        image: nginx
        ports:
        - containerPort: 80
EOF
    ```
  - `kubectl expose deployment/nginx`
  - Create a pod to test net access
    ``` bash
    $ kubectl run busybox --image=radial/busyboxplus:curl --command -- sleep 3600
    $ POD_NAME=$(kubectl get pods -l run=busybox -o jsonpath="{.items[0].metadata.name}")
    ```
  - Get the addresses for the nginx pods: `kubectl get ep nginx`
  - Check connectivity:
    ``` bash
    $ kubectl exec $POD_NAME -- curl <first nginx pod IP address>
    $ kubectl exec $POD_NAME -- curl <second nginx pod IP address>
    ```
  - Check that you can connect to services:
    ``` bash
    $ kubectl get svc
    $ kubectl exec $POD_NAME -- curl <nginx service IP address>
    ```