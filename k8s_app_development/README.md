
# Kubernetes Application Development

## K8S API primitives

- Sometimes called "Kubernetes Objects"
- Represent the state of the cluster
- Get all primitives: `kubectl api-resources -o name`
- All objects have the following attributes:
    - `spec`: Defines the desired state of an object
    - `status`: Current actual state of the object
- Get list of objects: `kubectl get ...`
    - `kubectl get pods`
    - `kubetl get node <nodename> -o yaml` - YAML output gives much more info than just `get`
- Describe: Get overview of specification and status. 
    - `kubectl describe <object> <instance_name>`

## Creating pods

- Pods consist of one or more containers and a set of resources shared by those containers.
- Example pod:
    ``` yaml
    # my-pod.yml

    apiVersion: v1  # Version of the K8S API
    kind: Pod  # Type of object
    metadata:
        name: my-pod
        labels:
            # Helps find your pods for later automation.
            app: myapp
    spec: # Define what you want.
        containers: # List of containers that should be run in this pod.
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
    ```
- Create it with: `kubectl create -f my-pod.yml`
- `kubectl get pods`
    - `READY` field shows the number of containers in the pod that are runnning.
- Update with `kubectl apply -f my-pod.yml`
    - or, make on-the-fly changes with: `kubectl edit pod my-pod`
- Delete the pod with: `kubectl delete pod my-pod

## Namespaces

- Organizes your cluster.
- All objects are assigned to a namespace. Default is `default`.
- Create a namespace with `kubectl create ns my-ns`
- When creating an object, you can assign a namespace with:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-ns-pod
        namespace: my-ns
    labels:
        app: myapp
    spec:
        containers:
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
    ```
- You need to specify the namespace when querying objects. `kubectl get pods` will
    only show the `default` NS.
    - Use `kubectl get pods -n my-ns`

## Container configuration

- `command` option: Override what the container runs.
- `args`: Arguments that should be passed to the `command`.
- `containerPort`: Ports are, by default, only accessible by containers
    in the same pod. Use this option to open a port up for the rest of the cluster.

## ConfigMaps

- Represents a key-value store of configuration data.
- you can update these maps on the fly, distrbuting the settings to everything that uses it.
- Example configmap:
    ``` yaml
    # my-config-map.yaml
    apiVersion: v1
    kind: ConfigMap
    metadata:
        name: my-config-map
    data:
        myKey: myValue
        anotherKey: anotherValue
    ```
- Use it in a pod:
    - You can mount it as an env var
        ``` yaml
        apiVersion: v1
        kind: Pod
        metadata:
            name: my-configmap-pod
        spec:
            containers:
            - name: myapp-container
                image: busybox
                command: ['sh', '-c', "echo $(MY_VAR) && sleep 3600"]
                env:
                - name: MY_VAR
                valueFrom:
                    configMapKeyRef:
                        name: my-config-map
                        key: myKey
        ```
    - Or as a mounted volume:
        ``` yaml
        apiVersion: v1
        kind: Pod
        metadata:
            name: my-configmap-volume-pod
        spec:
            containers:
            - name: myapp-container
                image: busybox
                command: ['sh', '-c', "echo $(cat /etc/config/myKey) && sleep 3600"]
                volumeMounts:
                - name: config-volume
                    mountPath: /etc/config
            volumes:
                - name: config-volume
                  configMap:
                    name: my-config-map
        ```

## Security context

- Define a pod's privelege and access control settings.
- For example, can enable operating system permissions.
- Defined as part of a pod's spec.
- Example, run as a specific user on the node:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-securitycontext-pod
    spec:
        securityContext:
            runAsUser: 2001
            fsGroup: 3001
        containers:
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', "cat /message/message.txt && sleep 3600"]
            volumeMounts:
            - name: message-volume
              mountPath: /message
        volumes:
        - name: message-volume
          hostPath:
            path: /etc/message
    ```

## Resource requirements

- Define and request memory/cpu usage
- Govern which worker nodes the pod will be scheduled on.
- Can also define resource limits. If a pod goes above this, it will get killed.
- Example:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-resource-pod
    spec:
    containers:
    - name: myapp-container
        image: busybox
        command: ['sh', '-c', 'echo Hello Kubernetes! && sleep 3600']
        resources:
            requests:
                memory: "64Mi"
                cpu: "250m"  # "MilliCPUs - 1/1000th of a CPU core.
            limits:
                memory: "128Mi"
                cpu: "500m"
    ```

## Secrets

- Allows you to store sensitive data in your cluster.
- Example:
    ``` yaml
    apiVersion: v1
    kind: Secret
    metadata:
        name: my-secret
    stringData:
        myKey: myPassword
    ```
- Delete the `.yaml` file containing the secret after creating the secret.
- Use the secret as an env var:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-secret-pod
    spec:
        containers:
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', "echo Hello, Kubernetes! && sleep 3600"]
            env:
            - name: MY_PASSWORD
              valueFrom:
                secretKeyRef:
                    name: my-secret
                    key: myKey
    ```

## Service account

- Allows containers in pods to access the K8S API
- Create service accouts that define the portions of the API that a pod shuold be allowed to access.
- Example: `kubectl create serviceaccount my-serviceaccount`
- Use it with:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-serviceaccount-pod
    spec:
        serviceAccountName: my-serviceaccount
        containers:
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', "echo Hello, Kubernetes! && sleep 3600"]
    ```

## Multi-container pods

- Pods with more than one containers.
- Example:
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: multi-container-pod
    spec:
    containers:
    - name: nginx
      image: nginx:1.15.8
      ports:
        - containerPort: 80
    - name: busybox-sidecar
      image: busybox
      command: ['sh', '-c', 'while true; do sleep 30; done;']
    ```
- Three ways in-pod containers can communicate with eachother
    - Shared network
    - Shared volumes
    - Shared process namespace
        - requires `shareProcessNamespace: true`
- Design patterns
    - Sidecar
        - Add a container to enhance or add functionality to the main container.
        - E.g., sync files from a git repo.
    - Ambassador pod
        - Capture and translate network traffic.
    - Adapter pod
        - Change output from main container
        - E.g., decorate log output.

## Liveness and readiness probes

- Probes: Customize how K8S determines status of containers.
- Liveness: Is the container running properly?
    - Defines when k8s will restart the container
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-liveness-pod
    spec:
        containers:
        - name: myapp-container
            image: busybox
            command: ['sh', '-c', "echo Hello, Kubernetes! && sleep 3600"]
            livenessProbe:
                exec:
                    command:
                    # Wait until the terminal is up and running.
                    - echo
                    - testing
                initialDelaySeconds: 5  # Time to wait until running the first probe.
                periodSeconds: 5  # Number of seconds to wait between probes.
    ```

- Readiness: Ready to service requests?
    - Decides whether requests will be forwarded to the pod.
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-readiness-pod
    spec:
        containers:
        - name: myapp-container
            image: nginx
            readinessProbe:
                httpGet:
                    path: /
                    port: 80
                initialDelaySeconds: 5
                periodSeconds: 5
    ```
        
- Probes can be implemented with, for example, commands and HTTP requests (more options available)

## Container logging

- By default, output will be sent into the container log.
- Can be accessed with `kubectl logs <pod> -c <container>`

## Installing the Metrics server

- This is just LinuxAcademy-specific stuff
- Gives extra info about a pod's memory usage and CPU
- Install:
    ``` yaml
    cd ~/
    git clone https://github.com/linuxacademy/metrics-server
    kubectl apply -f ~/metrics-server/deploy/1.8+/
    ```
- Check if it's responsive with: `kubectl get --raw /apis/metrics.k8s.io/`

## monitoring applications

- Requires Metric server
- `kubectl top` gathers info about resource usage in the cluster.
- `kubectl top pod <pod name>`
- `kubectl top pods -n <namespace>`
- `kubectl top nodes`

## Labels, selectors, annotations

- Labels: Key-value pairs attached to objects. Used to identify attributes of objects.
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-production-label-pod
        labels:
            app: my-app
            environment: production
    spec:
        containers:
        - name: nginx
            image: nginx
    ```
- Use labels with Selectors
    - Equality:
        - `kubectl get pods -l app=my-app`
        - `kubectl get pods -l app!=my-app`
    - Set-based selectors:
        - `kubectl get pods -l 'environment in (production,development)'
    - Multiple selectors:
        - `kubectl get pods -l app=my-app,environment=production`
- Annotations
    - K/V pairs, just like Labels.
    - They're not intended to be identifyers. Can't be used in selectors.
    - Basically just provide metadata for, for example, other automation tools
    ``` yaml
    apiVersion: v1
    kind: Pod
    metadata:
        name: my-annotation-pod
        annotations:
            owner: person@company.com
            git-commit: bdab0c6
    spec:
        containers:
        - name: nginx
            image: nginx
    ```

## Deployments

- Provide a way to declartively manage a dynamic set of pods

``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3  # Number of pods that should be in this deployment
  selector:  # Determine which pods should be considered part of this deployment.
    matchLabels:
      app: nginx
  template:  # Pod definition describing what should be run.
    metadata:  # Make sure that these match the selector.
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

## Rolling updates and rollbacks

- Gradually update replicas
- E.g., change the image of a deployment with: `kubectl set image deployment/rolling-update container_name new_image:1.4 --record`
    - `--record` will allow us to rollback later.
- Rollback with:
    - Get list of updates: `kubectl rollout history deployment/<name>`
    - Get more detailed info about a specific revision: `kubectl rollout history deployment/<name> --revision=<rev-number>`
    - Run a rollback to the previous version with: `kubectl rollout undo deployment/<name>`
    - Rollback to a specific revision with: `kubectl rollout undo deployment/<name> --to-revision=1`

## Jobs and CronJobs

- Jobs are used to execute a workload until its done (as opposed to, as with a traditional
    pod, running it forever).
    ``` yaml
    apiVersion: batch/v1
    kind: Job
    metadata:
        name: pi
    spec:
        template:
          spec:
            containers:
            - name: pi
              image: perl
              command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
          restartPolicy: Never
        backoffLimit: 4  # How many times should this job try to run?
    ```

- CronJob: Run a job on a schedule.
    ``` yaml
    apiVersion: batch/v1beta1
    kind: CronJob
    metadata:
        name: hello
    spec:
        schedule: "*/1 * * * *"
        jobTemplate:
            spec:
                template:
                    spec:
                        containers:
                        - name: hello
                            image: busybox
                            args:
                            - /bin/sh
                            - -c
                            - date; echo Hello from the Kubernetes cluster
                        restartPolicy: OnFailure
    ```

## Services

- Create an abstraction layer that provides network access to a dynamic set of pods
- Most services use a selector to find the pods that shuold receive traffic through
    the service.
- This means that, as pods (dis)appear, a service allows the client to continue
    sending requests.

- Create a deployment
``` yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        ports:
        - containerPort: 80
```

- Expose the pods with a service
``` yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  type: ClusterIP
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 80
```

- Get the list of places you can request a service with: `kubectl get endpoints my-service`
- Types of services:
    - ClusterIP: Expose within the cluster using an internal IP address. Also accessible using cluster's DNS.
    - NodePort: Expose externally via a port that listens on each node in the cluster.
        - Commonly used for testing.
    - LoadBalancer:
        - Only works if you're using a cloud provider. The service is exposed through a load
            balancer using the cloud platform.
    - ExternalName:
        - Maps the serviec to an external address.
        - Used to allow resource within the cluster to access things outside the cluster through a service.
        - Just sets up a DNS mapping. Does not proxy traffic.

## Network policies

- What traffic is allowed to come into and go out of your pods?
- Networking like Flannel requires something like Canal to be able to do this.

``` yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: my-network-policy
spec:
  podSelector:
    matchLabels:
      app: secure-app
  policyTypes:
  - Ingress
  - Egress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          allow-access: "true"
    ports:
    - protocol: TCP
      port: 80
  egress:
  - to:
    - podSelector:
        matchLabels:
          allow-access: "true"
    ports:
    - protocol: TCP
      port: 80
```

- Selector types:
    - `podSelector`: Select on a per-pod basis (e.g., by labels)
    - `namespaceSelector`:
        - Select by namespace
        - If this is present along with `podSelector`, then both must match
    - `ipBlock`: CIDR range

## Volumes

``` yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-pod
spec:
  containers:
  - image: busybox
    name: busybox
    command: ["/bin/sh", "-c", "while true; do sleep 3600; done"]
    volumeMounts:
    # Location within the container.
    - mountPath: /tmp/storage
      name: my-volume
  volumes:
  # Volumes that should be available to this pod.
  - name: my-volume
    # Create an empty directory on the node where the pod is created.
    emptyDir: {}
```

## Persistent volumes

- K8S is designed to maintain stateless containers.
- However, K8S defines primitives that let you store things permantently, then
    bind those resources to pods.
- Persistent volume
    - Represents a storage resource.
``` yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: my-pv
spec:
  storageClassName: local-storage
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data"
```

- Persistent volume claim
    - Abstraction layer between the user (the pod) of a storage resource and the PV>

``` yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  storageClassName: local-storage
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 512Mi
```

- Use the PVC with:
``` yaml
kind: Pod
apiVersion: v1
metadata:
  name: my-pvc-pod
spec:
  containers:
  - name: busybox
    image: busybox
    command: ["/bin/sh", "-c", "while true; do sleep 3600; done"]
    volumeMounts:
    - mountPath: "/mnt/storage"
      name: my-storage
  volumes:
  - name: my-storage
    persistentVolumeClaim:
      claimName: my-pvc
```